import json

#----------
#load_json
#----------
def load_json(filename):
    with open(filename) as file:
        """
          The load_json file takes in a json file and reads the file and returns the file in json format.
        """
        jsn = json.load(file)
        file.close()
        return jsn

#------------
#TypeBinds
#------------
def TypeBinds():
        '''
          Type Binds loads the type json file using the load_json function and then creates a dictionary
          that uses the chinese name as the key, which is unicode, and the english name as the value.
          It returns the dictionary type_dict
        '''
        typedata = load_json("./data/types.json")
        type_dict = {}
        for i in range(len(typedata)):
                type_dict[typedata[i]['cname']] = typedata[i]['ename']
        return(type_dict)
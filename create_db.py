import json
from models import db, Pokemon, Type, Skill
from TypeBinder import TypeBinds

#-----------
#load_json
#-----------
def load_json(filename):
    with open(filename) as file:
        """
          The load_json file takes in a json file and reads the file and returns the file in json format.
        """
        jsn = json.load(file)
        file.close()
        return jsn

#--------------
#create_pokemon
#--------------
def create_pokemon():
    """
        This function imports the data from pokedex json file from the load json function
        and binds it to the objects created in the
        pokemon model and commits the data to the postgres db.
        The function finds the defense, name, attack, hp, pokemon_id, speed, spatk, spdef,
        the type of the pokemon, and the image link from github where we got the data from.
    """
    pokemondata = load_json('./data/pokedex.json')
    for i in range(len(pokemondata)):
        defense = pokemondata[i]['base']['Defense']
        name = pokemondata[i]['ename']
        attack = pokemondata[i]['base']['Attack']
        hp = pokemondata[i]['base']["HP"]
        pokemon_id = pokemondata[i]['id']
        speed = pokemondata[i]['base']['Speed']
        spatk = pokemondata[i]['base']['Sp.Atk']
        spdef = pokemondata[i]['base']['Sp.Def']
        ptype = pokemondata[i]['type'][0]
        image_link = "https://github.com/fanzeyi/Pokemon-DB/blob/master/img/" + pokemon_id + name + ".png?raw=true"
        
        '''
        TypeBinds contains a function that stores the type IDs as keys and the type names as values.
        Then the for loop iterates through the type id that was provided by json and then assigns the 
        name for the type of that pokemon.
        '''
        type_dict = TypeBinds()
        if ptype in type_dict:
            ptype = type_dict[ptype]

        NewPokemon = Pokemon(name = name, attack = attack, defense = defense, hp = hp, spatk = spatk, spdef = spdef, spd = speed, pokemon_type_id = ptype, pokemon_id = pokemon_id, image_link = image_link)


        db.session.add(NewPokemon)
        # commit the session to my DB.
        db.session.commit()

#-------------
#create_skills
#-------------
def create_skills():
    """
       This function imports the data from skills json file from the load json function
       and binds it to the objects created in the
       Skill model and commits the data to the postgres db.
       It finds the skills id, name, pp, power, accuracy, and what type the skill is.
    """
    skillsdata = load_json('./data/skills.json')
    for i in range(len(skillsdata)):
        iddata = skillsdata[i]['id']
        name = (skillsdata[i]['ename'])
        pp = skillsdata[i]['pp']
        power = skillsdata[i]['power']
        accuracy = skillsdata[i]['accuracy']
        stype = skillsdata[i]['type']

        '''
        TypeBinds contains a function that stores the type IDs as keys and the type names as values.
        Then the for loop iterates through the type id that was provided by json and then assigns the 
        name for the type of that skill.
        '''
        type_dict = TypeBinds()
        for key in type_dict.keys():
            if key.startswith(stype):
                stype = type_dict[key]

        NewSkill = Skill(id = iddata, name = name, accuracy = accuracy, power = power, pp = pp, skill_type_id = stype)

        db.session.add(NewSkill)
        # commit the session to my DB.
        db.session.commit()

#------------
#create_types
#------------
def create_types():
    """
        This function imports the data from the type json file from the load json function
        and binds it to the objects created in the
        Type model and commits the data to the postgres db.
        It finds the Type name, what the type is weak and strong against, and
        what type it has no effect on, the generation released,
        and the image link from the second github profile we used.
    """
    typedata = load_json('./data/types.json')
    for i in range(len(typedata)):
        name = typedata[i]['ename']
        weak = typedata[i]['Weak_Against']
        strong = typedata[i]["Strong_Against"]
        noEffect = typedata[i]["no_effect"]
        generation = typedata[i]["gen_released"]
        image_url = "https://github.com/jayden10f/PokemonType/blob/master/PokemonTypes/" + name + "Type.jpg?raw=true"
        NewType = Type(name = name, strong_against = strong, weak_against = weak, no_effect = noEffect, gen_released= generation, image_link = image_url)

        db.session.add(NewType)
        # commit the session to my DB.
        db.session.commit()


create_types()
create_pokemon()
create_skills()



from flask import Flask, render_template, request
from models import app, db, Pokemon, Type, Skill
from create_db import create_pokemon, create_skills, create_types
from flask_paginate import Pagination, get_page_parameter
import tests
import unittest
from io import StringIO
import subprocess
from math import ceil

"""
	Variables that help with pagination, including the 
	number of items to include on each page and the 
	number of total count for each model 
"""
PER_PAGE = 10
NUM_POKEMON = db.session.query(Pokemon).count()
NUM_TYPES = db.session.query(Type).count()
NUM_SKILLS = db.session.query(Skill).count()

@app.route('/')
#-------------
#index
#-------------
def index():
	return render_template('index.html')


@app.route('/pokemonSearchResults/', methods = ['POST', 'GET'])
#--------------------
#pokemonSearchResults
#--------------------
def pokemonSearchResults():
	"""
		pokemonSearchResults is called when the pokemon form located in the 
		navigation bar is submitted. This function puts the request form in 
		the form variable, gets the correct requested page, queries the search 
		results using the contains function to compare every item in the 
		pokemon table to the entered string and itemizes these results so 
		that they can be paginated. Rendered templates is then called which
		directs the program to the pokemon.html with the queried search 
		results 
	"""
	if request.method == 'POST':
		form = request.form
		page = request.args.get(get_page_parameter(), type=int, default=1)
		searchResults = db.session.query(Pokemon).filter(Pokemon.name.contains(form['searchString'].capitalize())).paginate(page, PER_PAGE, False).items
		total = db.session.query(Pokemon).filter(Pokemon.name.contains(form['searchString'].capitalize())).count()
		total_pages = ceil(total / 10)
		ten_page = 1
		#pagination = Pagination(page=page, per_page = PER_PAGE, total=total, record_name='searchResults')
		return render_template('pokemon.html', pokemon = searchResults, page=page, per_page = PER_PAGE, total_pages = total_pages, query = form["searchString"], ten_page = ten_page)

@app.route("/pokemonSearchQuery/?<int:page>&", defaults={'query': ""})
@app.route("/pokemonSearchQuery/?<int:page>&<string:query>")
def pokemonSearchQuery(page, query):
	"""
		pokemonSearchQuery is called when the user navigates to a new page number
		after the inital POST form request for searching. The function takes in the page
		number and an optional query string that is used to query the database.
		The function then finds the searched for pokemon, selects 10 adjacent ones based on 
		the page number. It then returns the "pokemon.html" with the 10 pokemon and the 
		meta data for the page like the query string, page number, and total pages so the pagination works.
	"""
	
	total = db.session.query(Pokemon).filter(Pokemon.name.contains(query.capitalize()))

	total_pages = ceil(total.count() / PER_PAGE)

	if (page == 1):
		pokemon = total[:PER_PAGE]

	elif (page == total_pages):
		pokemon = total[(page - 1) * PER_PAGE :]

	else:
		pokemon = total[(page - 1) * PER_PAGE : (page - 1) * PER_PAGE + 10]

	return render_template("pokemon.html", pokemon=pokemon, page=page, per_page=PER_PAGE, total_pages=total_pages, query=query)

@app.route('/skillSearchResults/', methods = ['POST', 'GET'])
#--------------------
#skillSearchResults
#--------------------
def skillSearchResults():
	"""
		skillSearchResults is called when the skill form located in the 
		navigation bar is submitted. This function puts the request form in 
		the form variable, gets the correct requested page, queries the search 
		results using the contains function to compare every item in the 
		skill table to the entered string and itemizes these results so 
		that they can be paginated. Rendered templates is then called which
		directs the program to the skill.html with the queried search 
		results 
	"""
	if request.method == 'POST':
		form = request.form
		page = request.args.get(get_page_parameter(), type=int, default=1)
		searchResults = db.session.query(Skill).filter(Skill.name.contains(form['searchString'].capitalize())).paginate(page, PER_PAGE, False).items
		total = db.session.query(Skill).filter(Skill.name.contains(form['searchString'].capitalize())).count()
		total_pages = ceil(total / PER_PAGE)
		return render_template('skills.html', skills = searchResults, page=page, per_page = PER_PAGE, total_pages=total_pages, query=form["searchString"])

@app.route("/skillSearchQuery/?<int:page>&", defaults={'query': ""})
@app.route("/skillSearchQuery/?<int:page>&<string:query>")
def skillSearchQuery(page, query):
	"""
		skillSearchQuery is called when the user navigates to a new page number
		after the inital POST form request for searching. The function takes in the page
		number and an optional query string that is used to query the database.
		The function then finds the searched for skill, selects 10 adjacent ones based on 
		the page number. It then returns the "skill.html" with the 10 skill and the 
		meta data for the page like the query string, page number, and total pages so the pagination works.
	"""
	
	total = db.session.query(Skill).filter(Skill.name.contains(query.capitalize()))

	total_pages = ceil(total.count() / PER_PAGE)

	if (page == 1):
		skills = total[:PER_PAGE]

	elif (page == total_pages):
		skills = total[(page - 1) * PER_PAGE :]

	else:
		skills = total[(page - 1) * PER_PAGE : (page - 1) * PER_PAGE + 10]

	return render_template('skills.html', skills = skills, page=page, per_page = PER_PAGE, total_pages=total_pages, query=query)


@app.route('/typeSearchResults/', methods = ['POST', 'GET'])
#--------------------
#typeSearchResults
#--------------------
def typeSearchResults():
	"""
		typeSearchResults is called when the type form located in the 
		navigation bar is submitted. This function puts the request form in 
		the form variable, gets the correct requested page, queries the search 
		results using the contains function to compare every item in the 
		type table to the entered string and itemizes these results so 
		that they can be paginated. Rendered templates is then called which
		directs the program to the type.html with the queried search 
		results 
	"""
	if request.method == 'POST':
		form = request.form
		page = request.args.get(get_page_parameter(), type=int, default=1)
		searchResults = db.session.query(Type).filter(Type.name.contains(form['searchString'].capitalize())).paginate(page, PER_PAGE, False).items
		total = db.session.query(Type).filter(Type.name.contains(form['searchString'].capitalize())).count()
		total_pages = ceil(total / PER_PAGE)
		return render_template('types.html', types = searchResults, page=page, per_page = PER_PAGE, total_pages=total_pages, query=form["searchString"])

@app.route("/typeSearchQuery/?<int:page>&", defaults={'query': ""})
@app.route("/typeSearchQuery/?<int:page>&<string:query>")
def typeSearchQuery(page, query):
	"""
		typeSearchQuery is called when the user navigates to a new page number
		after the inital POST form request for searching. The function takes in the page
		number and an optional query string that is used to query the database.
		The function then finds the searched for types, selects 10 adjacent ones based on 
		the page number. It then returns the "type.html" with the 10 type and the 
		meta data for the page like the query string, page number, and total pages so the pagination works.
	"""
	
	total = db.session.query(Type).filter(Type.name.contains(query.capitalize()))

	total_pages = ceil(total.count() / PER_PAGE)

	if (page == 1 and page != total_pages):
		types = total[:PER_PAGE]

	elif (page == total_pages):
		types = total[(page - 1) * PER_PAGE :]

	else:
		types = total[(page - 1) * PER_PAGE : (page - 1) * PER_PAGE + 10]

	return render_template('types.html', types = types, page=page, per_page = PER_PAGE, total_pages=total_pages, query=query)


@app.route('/about/')
#----------
#about
#----------
def about():

	suite = unittest.TestLoader().loadTestsFromModule(tests)
	string_store = StringIO("Test cases")
	
	unittest.TextTestRunner(verbosity=2, stream=string_store).run(suite)

	results = str(string_store.getvalue())
	results = results.split("\n")

	return render_template('about.html', results = results)


@app.route('/pokemon/')
#----------
#pokemon
#----------
def pokemon():
	"""
		pokemon is called when the user clicks on a link to the pokemon model page. In the function
		the page number is set, using an imported function get_page_parameter() (with a default page number
		of 1). Using the connected database, we query all the Pokemon and call the 
		inherited paginate function on the queried result. The paginate function  seperates 
		the queried items into each perspective page (based on our passed in values). In addition to this, 
		we create a pagination object so that the page choices can be displayed on the actual HTML page. 
		The function returns the itemized pokemon, the page number, the number of items to display per 
		page and the pagination object, which is all to be displayed on 'pokemon.html' 
	"""
	page = request.args.get(get_page_parameter(), type=int, default=1)
	my_pokemon = db.session.query(Pokemon).paginate(page, PER_PAGE, False).items

	total_pages = ceil(NUM_POKEMON / PER_PAGE)
	ten_page = 1
	return render_template('pokemon.html', pokemon = my_pokemon, page = page, per_page = PER_PAGE, total_pages=total_pages, query="", ten_page = ten_page)


@app.route('/types/')
#------
#types
#------
def types():
	"""
		types is called when the user clicks on a link to the types model page. In the function
		the page number is set, using an imported function get_page_parameter() (with a default page number
		of 1). Using the connected database, we query all the Types and call the 
		inherited paginate function on the queried result. The paginate function  seperates 
		the queried items into each perspective page (based on our passed in values). In addition to this, 
		we create a pagination object so that the page choices can be displayed on the actual HTML page. 
		The function returns the itemized types, the page number, the number of items to display per 
		page and the pagination object, which is all to be displayed on 'types.html'
	"""
	page = request.args.get(get_page_parameter(), type=int, default=1)
	types = db.session.query(Type).paginate(page, PER_PAGE, False).items
	total_pages = ceil(NUM_TYPES / PER_PAGE)
	return render_template('types.html', types=types, page=page, per_page=PER_PAGE, total_pages=total_pages, query="")

@app.route('/skills/')
#--------
#skills
#--------
def skills():
	"""
		skills is called when the user clicks on a link to the skills model page. In the function,
		the page number is set, using an imported function get_page_parameter() (with a default page number
		of 1). Using the connected database, we query all the Skills and call the 
		inherited paginate function on the queried result. The paginate function  seperates 
		the queried items into each perspective page (based on our passed in values).In addition to this, 
		we create a pagination object so that the page choices can be displayed on the actual HTML page. 
		The function returns the itemized skills, the page number, the number of items to display per 
		page and the pagination object, which is all to be displayed on 'skills.html' 
	"""
	page = request.args.get(get_page_parameter(), type=int, default=1)
	skills=db.session.query(Skill).paginate(page,PER_PAGE, False).items
	total_pages = ceil(NUM_SKILLS / PER_PAGE)
	return render_template('skills.html', skills=skills, page=page, per_page= PER_PAGE, total_pages = total_pages, query = "")


@app.route('/pokemoninfo/<string:name>')
#-------
#pokemoninfo
#-------
def pokemoninfo(name):
	"""
		pokemoninfo takes in the name of a pokemon passed by a url using flask. Then the function queries the database
		for pokemon and then filters the query by what name was passed to pokemoninfo, which is pokemonname, 
		and the name of the pokemon and grabs the first row returned. Then returns the pokemoninfo template with 
		the pokemon returned from the query.
	"""
	p = db.session.query(Pokemon).filter_by(name = name).first()
	return render_template('pokemoninfo.html', pokemon = p)

@app.route('/skillinfo/<string:skillname>')
#---------
#skillinfo
#---------
def skillinfo(skillname):
	"""
		skillinfo takes in the name of a skill passed by a url using flask. Then the function queries the database
		for skill and then filters the query by what name passed to skillinfo, which is skillname, and the name of
		the skills and grabs the first row returned. Then returns the skillinfo template with the type returned from
		the query.
	"""
	s = db.session.query(Skill).filter_by(name = skillname).first()
	return render_template('skillinfo.html', skill = s)

@app.route('/typeinfo/<string:typename>')
#----------
#typeinfo
#----------
def typeinfo(typename):
	"""
		typeinfo takes in the name of a type passed by a url using flask. Then the function queries the database
		for type and then filters the query by what name passed to typeinfo, which is typename, and the name of
		the types and grabs the first row returned. Then returns the typeinfo template with the type returned from
		the query.
	"""
	t = db.session.query(Type).filter_by(name = typename).first()
	return render_template('typeinfo.html', type = t)

if __name__ == "__main__":
	app.run()


#----------------------------------------
# end of main2.py
#-----------------------------------------

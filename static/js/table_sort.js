function sortTable(column) {
    var table = document.getElementById("modelTable");
    var switching = true;
    var ascending = true;
    var loops = 0;
    var counts = 0;
    while (switching) {
        if (loops == 1 && counts == 0) {
            ascending = false;
        }
        if (loops == 2) {
            break;
        }
        var rows = table.rows;
        for (var i = 1; i < rows.length; i++) {
            for (var index = 1; index < rows.length - i; index++) {
                var x = rows[index].getElementsByTagName("TD")[column];
                var y = rows[index + 1].getElementsByTagName("TD")[column];
                var first = void 0;
                var second = void 0;
                first = Number(x.innerHTML);
                second = Number(y.innerHTML);
                if (isNaN(first) || isNaN(second)) {
                    first = x.innerHTML.toLowerCase();
                    second = y.innerHTML.toLowerCase();
                }
                // Check to see if two rows should switch place based on direction
                if (ascending) {
                    if (first > second) {
                        rows[i].parentNode.insertBefore(rows[index + 1], rows[index]);
                        counts += 1;
                        switching = false;
                    }
                }
                else if (!ascending) {
                    if (first < second) {
                        rows[i].parentElement.insertBefore(rows[index + 1], rows[index]);
                        counts += 1;
                        switching = false;
                    }
                }
            }
        }
        loops += 1;
    }
}

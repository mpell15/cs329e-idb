import os
import sys
import unittest
from models import db, Pokemon, Skill, Type
from create_db import create_pokemon, create_skills, create_types

class DBTestCases(unittest.TestCase):

    def test_pokemon_update_delete(self):

        p = Pokemon()
        p.name = "PokeTest"
        p.attack = 50
        p.defense = 50
        p.hp = 50
        p.image_link = "https://test.com"
        p.pokemon_id = "1"
        p.pokemon_type = "Electricity"
        p.spatk = 100
        p.spd = 50
        p.spdef = 100 

        db.session.add(p)
        db.session.commit()

        r = db.session.query(Pokemon).filter_by(pokemon_id = '1').first()
        self.assertEqual(str(r.pokemon_id), '1')

        # Get the pokemon that was inserted and change the name
        new_name = "Updated Test"
        r.name = new_name
        db.session.commit()

        # Query for the update and test
        u = db.session.query(Pokemon).filter_by(pokemon_id = '1').first()
        self.assertEqual(str(u.name), "Updated Test")

        # Delete the pokemon
        db.session.delete(u)
        db.session.commit()

    def test_pokenmon_insert_read(self):
        p = Pokemon()
        p.name = "PokeTest"
        p.attack = 50
        p.defense = 50
        p.hp = 50
        p.image_link = "https://test.com"
        p.pokemon_id = "1"
        p.pokemon_type = "Electricity"
        p.spatk = 100
        p.spd = 50
        p.spdef = 100 

        db.session.add(p)
        db.session.commit()

        r = db.session.query(Pokemon).filter_by(pokemon_id = '1').first()
        self.assertEqual(str(r.pokemon_id), '1')

        db.session.delete(r)
        db.session.commit()

    def test_database_seeded(self):

        # Get all types
        t = db.session.query(Type).all()
        self.assertTrue(len(t) > 0)

        # Check to make sure types has pokemon
        p = t[0].pokemons
        self.assertTrue(len(p) > 0)

        # Ensure Pokemon has skills
        s = t[0].skills
        self.assertTrue(len(s) > 0)

    def test_skills_insert_read(self):
        s = Skill()
        s.accuracy = 100
        s.id = -1
        s.name = "Test Skill"
        s.power = 100
        s.pp = 100

        db.session.add(s)
        db.session.commit()


        skill_db = db.session.query(Skill).filter_by(id = -1).first()
        self.assertTrue(skill_db.name == "Test Skill")

        db.session.delete(skill_db)
        db.session.commit()

    def test_skills_update_delete(self):

        s = Skill()
        s.accuracy = 100
        s.id = -1
        s.name = "Test Skill"
        s.power = 100
        s.pp = 100

        db.session.add(s)
        db.session.commit()

        skill_db = db.session.query(Skill).filter_by(id = -1).first()
        self.assertTrue(skill_db.name == "Test Skill")

        # Get the Skill
        skill_db = db.session.query(Skill).filter_by(id = -1).first()

        # Update the Skill
        skill_db.name = "Test Skill 2"
        db.session.commit()

        # Check the updated skill
        updated_skill = db.session.query(Skill).filter_by(id = -1).first()
        self.assertTrue(updated_skill.name == "Test Skill 2")

        db.session.delete(updated_skill)
        db.session.commit()

        updated_skill = db.session.query(Skill).filter_by(id = -1).first()
        self.assertTrue(updated_skill == None)

    def test_types_insert_read(self):
        
        t = Type()
        t.gen_released = 1
        t.image_link = "https://test.com"
        t.name = "Test Type"
        t.no_effect = "None"
        t.strong_against = "None"
        t.weak_against = "None"

        db.session.add(t)
        db.session.commit()


        test_db = db.session.query(Type).filter_by(name = "Test Type").first()
        self.assertTrue(test_db.name == "Test Type")

        db.session.delete(test_db)
        db.session.commit()

    def test_types_update_delete(self):
        
        # Create the type
        t = Type()
        t.gen_released = 1
        t.image_link = "https://test.com"
        t.name = "Test Type"
        t.no_effect = "None"
        t.strong_against = "None"
        t.weak_against = "None"

        db.session.add(t)
        db.session.commit()

        # Get the Type
        type_db = db.session.query(Type).filter_by(name = "Test Type").first()

        # Update the Type
        type_db.name = "Test Type 2"
        type_db.no_effect = "Fire"
        db.session.commit()

        # Check the updated Type
        updated_type = db.session.query(Type).filter_by(name = "Test Type 2").first()
        self.assertTrue(updated_type.name == "Test Type 2")
        self.assertTrue(updated_type.image_link == "https://test.com")
        self.assertTrue(updated_type.no_effect == "Fire")

        db.session.delete(updated_type)
        db.session.commit()


if __name__ == '__main__':
    unittest.main()

FILES :=                              \
    Pdb.html                      \
    IDB1.log                       \
    Pdb.py                        \
    RunPdb.in                     \
    RunPdb.out                    \
    RunPdb.py                     \
    TestPdb.out                   \
    TestPdb.py

#    Pdb-tests/YourGitLabID-RunPdb.in   \
#    Pdb-tests/YourGitLabID-RunPdb.out  \
#    Pdb-tests/YourGitLabID-TestPdb.out \
#    Pdb-tests/YourGitLabID-TestPdb.py  \
#

ifeq ($(shell uname), Darwin)          # Apple
    PYTHON   := python3.5
    PIP      := pip3.5
    PYLINT   := pylint
    COVERAGE := coverage-3.5
    PYDOC    := pydoc3.5
    AUTOPEP8 := autopep8
else ifeq ($(shell uname -p), unknown) # Windows
    PYTHON   := python                 # on my machine it's python
    PIP      := pip3.5
    PYLINT   := pylint
    COVERAGE := coverage-3.5
    PYDOC    := python -m pydoc        # on my machine it's pydoc 
    AUTOPEP8 := autopep8
else                                   # UTCS
    PYTHON   := python3.5
    PIP      := pip3
    PYLINT   := pylint3
    COVERAGE := coverage-3.5
    PYDOC    := pydoc3.5
    AUTOPEP8 := autopep8
endif

models.html: models.py
	$(PYDOC) -w models

Pdb-tests:
	git clone https://gitlab.com/fareszf/cs329e-Pdb-tests.git

Pdb.html: Pdb.py
	$(PYDOC) -w Pdb

IDB3.log:
	git log > IDB3.log

RunPdb.tmp: RunPdb.in RunPdb.out RunPdb.py
	$(PYTHON) RunPdb.py < RunPdb.in > RunPdb.tmp
	diff --strip-trailing-cr RunPdb.tmp RunPdb.out

TestPdb.tmp: TestPdb.py
	$(COVERAGE) run    --branch TestPdb.py >  TestPdb.tmp 2>&1
	$(COVERAGE) report -m                      >> TestPdb.tmp
	cat TestPdb.tmp

check:
	@not_found=0;                                 \
    for i in $(FILES);                            \
    do                                            \
        if [ -e $$i ];                            \
        then                                      \
            echo "$$i found";                     \
        else                                      \
            echo "$$i NOT FOUND";                 \
            not_found=`expr "$$not_found" + "1"`; \
        fi                                        \
    done;                                         \
    if [ $$not_found -ne 0 ];                     \
    then                                          \
        echo "$$not_found failures";              \
        exit 1;                                   \
    fi;                                           \
    echo "success";

clean:
	rm -f  .coverage
	rm -f  *.pyc
	rm -f  RunPdb.tmp
	rm -f  TestPdb.tmp
	rm -rf __pycache__
	rm -rf cs329e-Pdb-tests

config:
	git config -l

format:
	$(AUTOPEP8) -i Pdb.py
	$(AUTOPEP8) -i RunPdb.py
	$(AUTOPEP8) -i TestPdb.py

scrub:
	make clean
	rm -f  Pdb.html
	rm -f  Pdb.log

status:
	make clean
	@echo
	git branch
	git remote -v
	git status
	
versions:
	which     $(AUTOPEP8)
	autopep8 --version
	@echo
	which    $(COVERAGE)
	coverage --version
	@echo
	which    git
	git      --version
	@echo
	which    make
	make     --version
	@echo
	which    $(PIP)
	pip      --version
	@echo
#	which    $(PYDOC)
#	pydoc    --version
#	@echo
	which    $(PYLINT)
	pylint   --version
	@echo
	which    $(PYTHON)
	python   --version

test: Pdb.html Pdb.log RunPdb.tmp TestPdb.tmp Pdb-tests check
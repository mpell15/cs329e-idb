#Pokemon site models
from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
import os

'''
    Because our project requires us to store information about pokemon, types, and skills, we needed to
    put all of this information in a database. Models.py tells the database what tables to build, so we
    can simply store objects of their respective class inside them, and treat the database like
    an object in our main app, without having to worry about writing actual SQL queries. Before running
    any of the table creations, however, our code always drops all existing tables in the database.
'''

#---------------
#Start flask app
#---------------
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:asd123@localhost:5432/pokedex2') # postgresql+psycopg2://postgres:asd123@/postgres?host=/cloudsql/school-220514:us-central1:pokedex'
#--------------------------------------------------
#Get connection string from database inside the app
#--------------------------------------------------
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
#----------------------------------------------------------------
#Variable to represent database, so it can be modified by the app
#----------------------------------------------------------------
db = SQLAlchemy(app)

#---------------------------------------------------------------------
#Pre-check condition to check ensure that database is not of type none
#---------------------------------------------------------------------
assert db != None

#-----------------------------------------------------------------------
#Pokemon class holds all relevant attributes to display for each pokemon
#-----------------------------------------------------------------------
class Pokemon(db.Model):
    #----------------------------------------------------------------
    #Each object is stored in a table named "Pokemon" in the database
    #----------------------------------------------------------------
    __tablename__ = 'pokemon'
    #-------------
    #Table headers
    #-------------
    pokemon_id = db.Column(db.String(20), primary_key = True)
    name = db.Column(db.String(20))
    attack = db.Column(db.Integer, nullable = True)
    defense = db.Column(db.Integer, nullable = True)
    hp = db.Column(db.Integer, nullable = True)
    spatk = db.Column(db.Integer, nullable = True)
    spdef = db.Column(db.Integer, nullable = True)
    spd = db.Column(db.Integer, nullable = True)
    #-----------------------
    #Navigational properties
    #-----------------------
    pokemon_type_id = db.Column(db.String(20), db.ForeignKey('type.name'), nullable = True)
    image_link = db.Column(db.String(500), nullable = True)

#-------------------------------------------------------------
#Types class holds all relevant attributes about pokemon types
#-------------------------------------------------------------
class Type(db.Model):
    #--------------------------------------------------------------
    #Each object is stored in a table called "Type" in the database
    #--------------------------------------------------------------
    __tablename__ = "type"
    #-------------
    #Table headers
    #-------------
    name = db.Column(db.String(20), primary_key = True)
    strong_against = db.Column(db.String(20), nullable = True)
    weak_against = db.Column(db.String(20), nullable = True)
    no_effect = db.Column(db.String(20), nullable = True)
    gen_released = db.Column(db.String(2), nullable = True)
    image_link = db.Column(db.String(500), nullable = True)
    #-----------------------
    #Navigational properties
    #-----------------------
    pokemons = db.relationship('Pokemon', backref = 'type', lazy = False)
    skills = db.relationship('Skill', backref = 'type', lazy = False)

#---------------------------------------------------------------
#Skills class holds all relevant attributes about pokemon skills
#---------------------------------------------------------------
class Skill(db.Model):
    #---------------------------------------------------------------
    #Each object is stored in a table called "skill" in the database
    #---------------------------------------------------------------
    __tablename__ = "skill"
    #-------------
    #Table headers
    #-------------
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(20), nullable = True)
    accuracy = db.Column(db.String(20), nullable = True)
    power = db.Column(db.String(20), nullable = True)
    pp = db.Column(db.String(20), nullable = True)
    #---------------------------------------------
    #Navigational property between skills and type
    #---------------------------------------------
    skill_type_id = db.Column(db.String(20), db.ForeignKey('type.name'), nullable = True)

#------------------------------------
#Drop all existing tables in database
#------------------------------------
db.drop_all()

#-------------------------------
#Create all tables defined above
#-------------------------------
db.create_all()

#-----------------------------------------------------------
#Post-check condition to ensure database is not of type none
#-----------------------------------------------------------
assert db != None
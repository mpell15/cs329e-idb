import typing

class Stats(object):

    # properties
    attack = 0
    defense = 0
    hp = 0
    special_attack = 0
    special_defend = 0
    speed = 0
    
    def __init__(self, _attack = 0, _defense = 0, _hp = 0, _special_attack = 0,_special_defense = 0, _speed = 0):
        self.attack = _attack
        self.defense = _defense
        self.hp = _hp
        self.special_attack = _special_attack
        self.special_defend = _special_defense
        self.speed = _speed

class Skills(object):
    egg = []
    level_up = []
    pre_evolution = []
    tm = []
    transfer = []
    tutor = []

    def __init__(self, _egg = [], _level_up = [], _pre_evolution = [], _tm = [], _transfer = [], _tutor = []):
        self.egg = _egg
        self.level_up = _level_up
        self.pre_evolution = _pre_evolution
        self.tm = _tm
        self.transfer = _transfer
        self.tutor = _tutor

class Pokemon(object):

    stats = Stats()
    skills = Skills()
    cname = ""
    ename = ""
    id = ""
    jname = ""
    type = []
    
    def __init__(self, _stats = None, _skills = None, _cname = "", _ename = "", _id = "", _jname = "", _type = []):
        self.stats = _stats
        self.skills = _skills
        self.cname = _cname
        self.ename = _ename
        self.id = _id
        self.jname = _jname
        self.type = _type
        